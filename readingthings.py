# Need to use both readline() and readlines()

def readingthings(filename):
    with open(filename, 'r') as f:
        readstuff = f.readline()
        print('Result of readline')
        print(readstuff)
        print(len(readstuff))
        stripped = readstuff.rstrip()
        print(stripped)
        print(len(stripped))
        f.close()
    with open (filename, 'r') as f:
        readstuffs = f.readlines()
        print('Results of readlines')
        print(readstuffs)
        print(len(readstuffs))
        f.close()

if __name__ == '__main__':
    readingthings('example_text.txt')